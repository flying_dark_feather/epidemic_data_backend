# vue各城市疫情数据可视化后端

## 重要：由于木小果接口平台关闭，目前接口丢失，平台不能正常运行，
## 非常抱歉，后来可能会通过爬取第三方接口补救。
#### 1.项目描述
​	基于koa框架的后端接口

#### 2.任务点描述

（1）实时拿到某省数据（新浪）

（2）实时拿到某省的内部情况（推荐新浪）

（3）实时拿到危险地区的经纬度（卫健委）

（4）获取某省自疫情以来的数据（木小果，内部缓存）

（5）获取某省当前的危险地区（文字）（木小果）

（6）获取全国新闻信息（木小果）

（7）获取全国危险地区（文字）（天行数据）

（8）获取某大洲的数据（天行数据）

（9）获取全球各国家疫情信息（天行数据）

flying_dark_feather：（4）（6）（8）（9）

ohlayman：（1）（2）（3）（5）（7）

