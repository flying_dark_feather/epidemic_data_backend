const koa = require("koa");
const app = new koa();
const initProject = require("./src/config");
initProject(app);
app.listen(3030, () => {
  console.log("localhost:3030");
});
