const router = require("koa-router")();
const getDanger = require("../models/get/danger");
const getNews = require("../models/get/news");
const getUpdate = require("../models/get/update");
const getNowProvince = require("../models/get/province-now");
const getForeign = require("../models/get/foreign");
// const getArea = require("../models/get/area");
const getIncrease = require("../models/get/increase");
const getIndex = require("../models/get/index");
const getHome = require("../models/get/home");
const getTest = require("../models/get/test");
const getLogin = require("../models/get/login");
const getInterFace = require("../models/get/interface");
const getAbout = require("../models/get/about");
const getJournal = require("../models/get/journal");
const getPublicKey = require("../models/get/publicKey");
// const getAdd = require("../models/get/add");
// const getEdit = require("../models/get/edit");
const postUpdate = require("../models/post/update");
const postLogin = require("../models/post/login");
const postOpen = require("../models/post/open");
const postClose = require("../models/post/close");
//后台首页
router.get("/", getHome);
router.get("/admin", getHome);
//用户登录
router.get("/admin/login", getLogin);
//后台首页
router.get("/admin/index", getIndex);
//接口管理
router.get("/admin/interface", getInterFace);
//信息设置
router.get("/admin/journal", getJournal);
//关于我们
router.get("/admin/about", getAbout);
//关于我们
// router.get("/admin/add", getAdd);
// //编辑用户
// router.get("/admin/edit", getEdit);
//更新数据库
router.get("/admin/update", getUpdate);
//获取公钥
router.get("/v1/publicKey", getPublicKey);
//获取风险地区
router.get("/v1/danger", getDanger);
//获取新闻列表
router.get("/v1/news", getNews);
//获取当前省市信息
router.get("/v1/provinces/now", getNowProvince);
//获取国外疫情信息
router.get("/v1/foreign", getForeign);
//获取国内风险地区经纬度(因接口和实际使用情况问题关闭)
// router.get("/v1/area", getArea);
//获取最近60天疫情数据
router.get("/v1/increase", getIncrease);
//更新数据库
router.post("/v1/update", postUpdate);
//用户登录
router.post("/v1/login", postLogin);
//测试路由
router.get("/v1/test", getTest);
//开启同步
router.post("/v1/open", postOpen);
//关闭同步
router.post("/v1/close", postClose);
module.exports = router;
