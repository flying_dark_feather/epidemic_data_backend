const { connection } = require("../base/connect");
const { decryptCode } = require("../../utils/crypto");
const { sendData } = require("../base/success");
const { throwError } = require("../base/error");
module.exports = async (ctx) => {
  const secretCode = ctx.request.body.secret_code;
  const userInfo = JSON.parse(decryptCode(secretCode));
  const searchObj = {
    username: userInfo.username,
    password: userInfo.password,
  };
  const collection = (await connection).collection("admin");
  const result = await collection
    .find(searchObj, { projection: { _id: 0 } })
    .toArray();
  if (!result.length) {
    ctx.cookies.set(
      "epidemic-info",
      JSON.stringify({
        username: userInfo.username,
        isLogin: true,
      }),
      { maxAge: 86400000, httpOnly: false } //设置过期时间一天
    );
    sendData(ctx, result, "登陆成功~");
  } else {
    throwError(ctx, -3, "用户名或密码错误~");
  }
};
