const { connection } = require("../base/connect");
const { sendData } = require("../base/success");
const { throwError } = require("../base/error");
module.exports = async (ctx) => {
  let success = 0;
  const collectionList = ctx.request.body.collection_id;
  const collection = (await connection).collection("collection");
  for (let i = 0; i < collectionList.length; i++) {
    let temp = await collection.updateOne(
      { collection_id: collectionList[i] },
      { $set: { state: "1" } }
    );
    success += temp.modifiedCount;
  }
  if (success == collectionList.length) {
    sendData(ctx, 0, "修改成功~");
  } else {
    throwError(ctx, -1, "更新同步状态失败~");
  }
};
