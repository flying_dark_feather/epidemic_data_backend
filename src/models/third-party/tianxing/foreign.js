//数据来源：天行数据
//刷新国外数据库
const https = require("https");
const refreshCollection = require("../manage/refresh");
module.exports = function refreshForeign(updateCallback, database, targetUrl) {
  let foreignData = "";
  https.get(targetUrl, (result) => {
    result.on("data", (chunk) => {
      foreignData += chunk;
    });
    result.on("end", () => {
      const resultList = JSON.parse(foreignData);
      if (resultList.code == 200) {
        const dataList = resultList.newslist;
        refreshCollection(updateCallback, database, "foreign", dataList);
      } else {
        updateCallback("fail", "foreign");
      }
    });
  });
};
