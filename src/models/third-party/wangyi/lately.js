//刷新最近60天数据
const https = require("https");
const refreshCollection = require("../manage/refresh");
module.exports = function refreshLately(updateCallback, database, targetUrl) {
  let latelyList = "";
  https.get(targetUrl, (result) => {
    result.on("data", (chunk) => {
      latelyList += chunk;
    });
    result.on("end", () => {
      const resultList = JSON.parse(latelyList);
      if (resultList.code == 10000) {
        resultList.data.chinaTotal.type = "whole";
        refreshCollection(
          updateCallback,
          database,
          "lately",
          resultList.data.chinaTotal,
          resultList.data.chinaDayList
        );
      } else {
        updateCallback("fail", "lately");
      }
    });
  });
};
