//获取标准格式的时间函数
function completeDate(value) {
  return value < 10 ? "0" + value : value;
}
function getTimeStamp() {
  return parseInt(new Date().getTime() / 1000);
}
function getCurrentTime() {
  let nowDate = new Date();
  const day = nowDate.getDate();
  const month = nowDate.getMonth() + 1; //注意月份需要+1
  const year = nowDate.getFullYear();
  const hour = nowDate.getHours();
  const minutes = nowDate.getMinutes();
  const seconds = nowDate.getSeconds();
  return (currentTime =
    "" +
    completeDate(year) +
    "-" +
    completeDate(month) +
    "-" +
    completeDate(day) +
    " " +
    completeDate(hour) +
    ":" +
    completeDate(minutes) +
    ":" +
    completeDate(seconds));
}
function getLastDay() {
  let targetDate = new Date();
  targetDate.setDate(targetDate.getDate() - 2); //数据更新会延迟两天
  const day = targetDate.getDate();
  const month = targetDate.getMonth() + 1; //注意月份需要+1
  const year = targetDate.getFullYear();
  return (dateStr =
    "" + completeDate(year) + completeDate(month) + completeDate(day));
}
module.exports = { getCurrentTime, getTimeStamp, getLastDay };
