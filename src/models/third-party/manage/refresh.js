module.exports = async function refreshCollection(
  callback,
  database,
  collectionName,
  ...arrayList
) {
  await database.collection(collectionName).deleteMany({});
  arrayList.forEach((item, index) => {
    if (!item.length) database.collection(collectionName).insertOne(item);
    else {
      database.collection(collectionName).insertMany(item, {
        writeConcern: 1,
        ordered: true,
      });
    }
    if (index == arrayList.length - 1) {
      callback && callback("success", collectionName);
    }
  });
};
