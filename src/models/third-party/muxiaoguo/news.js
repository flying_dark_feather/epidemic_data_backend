//刷新新闻数据库
const https = require("https");
const refreshCollection = require("../manage/refresh");
module.exports = function refreshNews(updateCallback, database, targetUrl) {
  let newsData = "";
  https.get(targetUrl, (result) => {
    result.on("data", (chunk) => {
      newsData += chunk;
    });
    result.on("end", () => {
      const resultList = JSON.parse(newsData);
      if (resultList.code == 200) {
        const dataList = resultList.data;
        refreshCollection(updateCallback, database, "news", dataList);
      } else {
        updateCallback("fail", "news");
      }
    });
  });
};
