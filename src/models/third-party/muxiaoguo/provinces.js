//刷新provinces数据库(木小果)
const https = require("https");
const refreshCollection = require("../manage/refresh");
module.exports = function refreshProvinces(
  updateCallback,
  database,
  targetUrl
) {
  let provincesData = "";
  https.get(targetUrl, (result) => {
    result.on("data", (chunk) => {
      provincesData += chunk;
    });
    result.on("end", () => {
      const resultList = JSON.parse(provincesData);
      if (resultList.code == 200) {
        const dataList = resultList.data;
        refreshCollection(updateCallback, database, "provinces", dataList);
      } else {
        updateCallback("fail", "provinces");
      }
    });
  });
};
