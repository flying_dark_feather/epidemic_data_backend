//数据来源来源：木小果
//刷新危险地区数据库
const https = require("https");
const refreshCollection = require("../manage/refresh");
module.exports = function refreshDanger(updateCallback, database, targetUrl) {
  let dangerData = "";
  https.get(targetUrl, (result) => {
    result.on("data", (chunk) => {
      dangerData += chunk;
    });
    result.on("end", () => {
      const resultList = JSON.parse(dangerData);
      if (resultList.code == 200) {
        const wholeInfo = new Object({
          hcount: resultList.data.hcount,
          mcount: resultList.data.mcount,
          end_update_time: resultList.data.end_update_time,
        });
        let dataList = new Array();
        resultList.data.highlist.forEach((item) => {
          item.type = "high";
          dataList.push(item);
        });
        resultList.data.middlelist.forEach((item) => {
          item.type = "middle";
          dataList.push(item);
        });
        refreshCollection(
          updateCallback,
          database,
          "danger",
          wholeInfo,
          dataList
        );
      } else {
        console.log("木小果调用失败，失败原因：", dangerData);
        updateCallback("fail", "danger");
      }
    });
  });
};
