//数据来源：新浪微博
//风险地区经纬度
const https = require("https");
const refreshCollection = require("../manage/refresh");
module.exports = function refreshForeign(updateCallback, database, targetUrl) {
  https.get(targetUrl, (result) => {
    let areaData = "";
    result.on("data", (chunk) => {
      areaData += chunk;
    });
    result.on("end", () => {
      const resultList = JSON.parse(areaData);
      console.log("输出数据：", resultList);
      return;
      if (resultList.type == "FeatureCollection") {
        const dataList = resultList.features;
        refreshCollection(updateCallback, database, "foreign", dataList);
        database.collection("area").deleteMany({});
        addUpdateTime("area");
        database.collection("area").insertMany(dataList);
      } else {
        updateCallback("fail", "foreign");
      }
    });
  });
};
