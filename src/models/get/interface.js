const { connection } = require("../base/connect");
module.exports = async (ctx) => {
  //获取用户信息
  const collection = (await connection).collection("collection");
  const interFaceList = await collection
    .find({}, { projection: { _id: 0 } })
    .toArray();
  await ctx.render("interface", {
    title: "疫情可视化后台接口管理",
    userList: interFaceList,
  });
};
