const { connection } = require("../base/connect");
const { throwError } = require("../base/error");
const { sendData } = require("../base/success");
module.exports = async (ctx) => {
  const collection = (await connection).collection("danger");
  // 风险地区类型参数
  let level = ctx.query["level"];
  // 没有传参数就默认为all
  if (!level) level = "all";
  if (["all", "high", "middle"].indexOf(level) == -1) {
    throwError(ctx, -1, "参数错误");
    return;
  } else {
    let result = new Object();
    if (level == "all") {
      result = await collection.find().toArray();
    } else {
      result = await collection.find({ type: `${level}` }).toArray();
    }
    sendData(ctx, result);
  }
};
