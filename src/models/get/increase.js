const { connection } = require("../base/connect");
const { throwError } = require("../base/error");
const { sendData } = require("../base/success");
module.exports = async (ctx) => {
  let collection = (await connection).collection("lately");
  // 拿到最近60天的疫情数据
  const result = await collection
    .find({}, { projection: { _id: 0, extData: 0, lastUpdateTime: 0 } })
    .toArray();
  if (result.length != 60) {
    throwError(ctx, -2, "数据异常");
  }
  sendData(ctx, result);
};
