module.exports = async (ctx) => {
  await ctx.render("index", {
    title: "疫情可视化后台用户首页",
  });
  // 设置允许跨域
  // ctx.set('Access-Control-Allow-Origin', '*');
  // ctx.set('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  // ctx.set('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
};
