const { getPublicKey } = require("../../utils/crypto");
const { sendData } = require("../base/success");
module.exports = async (ctx) => {
  sendData(ctx, {
    public_key: getPublicKey(),
    validate_time: new Date().getTime(),
  });
};
