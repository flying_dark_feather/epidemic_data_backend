//此处用来写测试接口，不会实际使用，后端开发人员可以任意修改
//当前使用的功能为木小果疫情相关接口开启了鉴权测试
// const https = require("https");
// const { md5 } = require("../../utils/crypto");
// module.exports = async (ctx) => {
//   const time = parseInt(new Date().getTime() / 1000);
//   const url = "https://api.muxiaoguo.cn/api/epidemic"; //API链接
//   console.log("输出时间戳：", time);
//   const time_stamp = "time_stamp=" + time; //10位时间戳
//   const secret =
//     "api_id=10000305&secretKey=a8e1fbb802ec9a141c0dd9a432b65acd" +
//     "&" +
//     time_stamp;
//   console.log("md5被加密字段：", secret);
//   const sign = md5(secret); //生成签名（Apiid、secretKey、time_stamp）组合生成MD5
//   const targetUrl =
//     url +
//     "?api_key=fa5d78ca7d58445d&sign=" +
//     sign +
//     "&" +
//     time_stamp +
//     "&type=epidemicRiskLevel";
//   console.log("输出targetUrl:", targetUrl);
//   https.get(targetUrl, (result) => {
//     let msg = "";
//     result.on("data", (chunk) => {
//       msg += chunk;
//     });
//     result.on("end", () => {
//       console.log("输出请求结果：", msg);
//     });
//   });
// };
//mongoDB连接池问题
// const dbUserName = "epidemic";
// const dbPassWord = "flying_dark_feather";
// const targetDataBase = "epidemic";
// const url = `mongodb://${dbUserName}:${dbPassWord}@8.136.14.176:2700/${targetDataBase}`;
// const { MongoClient } = require("mongodb");
// module.exports = async (ctx) => {
//   let isSuccess = true;
//   let success = 0;
//   let fail = 0;
//   let timer = setInterval(() => {
//     const client = new MongoClient(url);
//     client
//       .connect()
//       .then((res) => {
//         console.log("连接成功：", res);
//         success++;
//       })
//       .catch((err) => {
//         console.log("连接失败：", err);
//         isSuccess = false;
//         fail++;
//         clearInterval(timer);
//       })
//       .finally(() => {
//         console.log("完成" + success + "次测试", "失败" + fail + "次测试");
//         client.close();
//       });
//   }, 100);
// };
//数据加密测试
const fs = require("fs");
const path = require("path");
const NodeRSA = require("node-rsa");
module.exports = async (ctx) => {
  const PUBLIC_KEY = fs.readFileSync(
    path.resolve(__dirname, "../../cert/public.pem")
  );
  const PRIVATE_KEY = fs.readFileSync(
    path.resolve(__dirname, "../../cert/private.key")
  );
  //函数测试;
  const text = "hello world";
  console.log(
    "测试加密：",
    new NodeRSA(PUBLIC_KEY.toString()).encrypt(text, "base64")
  );
  console.log(
    "测试解密：",
    new NodeRSA(PRIVATE_KEY.toString()).decrypt(text, "utf8")
  );
  ctx.body = {
    type: "成功执行",
  };
};
//测试结束;
