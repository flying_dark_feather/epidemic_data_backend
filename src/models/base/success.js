// 请求成功接口数据的封装 包括默认参数
// const { closeConnect } = require("./connect");
function sendData(ctx, data, msg = "success", code = 0) {
  //   closeConnect();
  ctx.body = {
    code,
    msg,
    data,
  };
}
module.exports = { sendData };
