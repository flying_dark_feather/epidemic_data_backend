// 请求错误接口数据的封装 包括默认参数
// const { closeConnect } = require("./connect");
function throwError(ctx, errCode, msg) {
  //   closeConnect();
  ctx.body = {
    code: errCode,
    msg,
  };
}
module.exports = { throwError };
