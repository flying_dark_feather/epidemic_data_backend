//全局cookie管理
function getCookie(name) {
  var arr = document.cookie.split("; ");
  for (var i = 0, len = arr.length; i < len; i++) {
    var item = arr[i].split("=");
    if (item[0] == name) {
      return item[1];
    }
  }
  return "";
}
let cookieData = getCookie("epidemic-info");
const currentUrl = window.location.pathname;
if (cookieData != "") {
  cookieData = JSON.parse(cookieData);
  if (cookieData.isLogin) {
    if (
      currentUrl == "/admin/about" ||
      currentUrl == "/admin/index" ||
      currentUrl == "/admin/interface" ||
      currentUrl == "/admin/journal"
    )
      document.getElementsByClassName("admin-username")[0].innerHTML =
        cookieData.username;
    if (currentUrl == "/admin/login") {
      window.location.href = "/admin/index";
    }
  } else {
    if (currentUrl != "/admin/login") {
      window.location.href = "/admin/login";
    }
  }
} else {
  if (currentUrl != "/admin/login") {
    window.location.href = "/admin/login";
  }
}
